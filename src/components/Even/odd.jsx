import React from 'react';
import './odd.css'


const odd= () => {

    let arrs = []
    let odds = []
    let evens = []
    let nums = [100,102,104,105,106,110,111,105,120, 121,110,100]
    nums.sort()
    for(let i=0;i<nums.length;i++){
        if((nums[i]%2)===0){

            evens.push(nums[i])

            if(!evens.includes(nums[i-1]) && odds.length>0){
                arrs.push(odds)
                odds=[]
            }
        }else{

            odds.push(nums[i])
            if(!odds.includes(nums[i-1]) && evens.length>0){
                arrs.push(evens)
                evens=[]
            }
        }

        if(i===(nums.length - 1)){
            odds.length > 0 && arrs.push(odds)
            evens.length > 0 && arrs.push(evens)
        }
    }

    return (
        <div>
            <div className="Row  ">
                <table className=" oddEven" >
                    <thead>
                    <tr>
                    {
                        arrs.map(arr => {
                            if (arr[0] % 2 == 0) {
                                return (
                                        <th>Even</th>
                                )}
                            else {
                                return(
                             <th>Odd</th>
                                )}
                        })
                    }
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                            {
                                arrs.map(arr => {
                                    if (arr[0] % 2 == 0) {
                                        return (
                                            arr.map(num => {
                                                return (
                                                    <td> {num} </td>
                                                );
                                            }))
                                    }else{
                                        return (
                                            arr.map(num => {
                                                return (
                                                     <td> {num} </td>
                                                );
                                            }))
                                    }})
                            }
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    );
};
export default odd
